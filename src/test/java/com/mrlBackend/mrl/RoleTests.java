package com.mrlBackend.mrl;

import com.mrlBackend.mrl.model.RoleList;
import com.mrlBackend.mrl.response.BaseResponse;
import org.junit.jupiter.api.Test;
import org.springframework.web.client.RestTemplate;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class RoleTests {

    @Test
    public void addRoleTest() {
        RoleList roleList = getRoleList();
        RestTemplate restTemplate = new RestTemplate();
        final String url = "http://localhost:8080/api/v1/addUpdateRole";
        BaseResponse baseResponse = restTemplate.postForObject(url, roleList, BaseResponse.class);
        assertEquals("200", baseResponse.getCode());
    }

    private RoleList getRoleList() {
        RoleList roleList = new RoleList();
        roleList.setRoleName("Test_MPR_TEST");
        roleList.setStatus("A");
        return roleList;
    }
}
