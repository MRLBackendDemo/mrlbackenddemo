package com.mrlBackend.mrl;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MrlApplication {

	public static void main(String[] args) {
		SpringApplication.run(MrlApplication.class, args);
	}

}
