package com.mrlBackend.mrl.model;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "MRL_DMT_USER_ROLE_MAPPING")
public class UserRoleMapping {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    private int roleIds;

    private int userId;

    public UserRoleMapping() {}

    public UserRoleMapping(int id, int roleIds) {
        this.id = id;
        this.roleIds = roleIds;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getRoleIds() {
        return roleIds;
    }

    public void setRoleIds(int roleIds) {
        this.roleIds = roleIds;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserRoleMapping that = (UserRoleMapping) o;
        return id == that.id && roleIds == that.roleIds;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, roleIds);
    }

    @Override
    public String toString() {
        return "UserRoleMapping{" +
                "userId=" + id +
                ", roleIds=" + roleIds +
                '}';
    }
}
