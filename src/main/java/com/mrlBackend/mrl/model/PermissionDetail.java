package com.mrlBackend.mrl.model;

import javax.persistence.*;

@Entity
@Table(name = "MRL_DMT_PERMISSION_LIST")
public class PermissionDetail {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @Column(name = "catType")
    private String catType;

    @Column(name = "functionality")
    private String functionality;

    @Column(name = "pageId")
    private int pageId;

    @Column(name = "pageName")
    private String pageName;

    @Column(name = "roleId")
    private int roleId;

    @Column(name = "roleName")
    private String roleName;

    @Column(name = "status")
    private String status;

    public PermissionDetail() {
    }

    public PermissionDetail(String catType, String functionality, int pageId, String pageName, int roleId, String roleName, String status) {
        this.catType = catType;
        this.functionality = functionality;
        this.pageId = pageId;
        this.pageName = pageName;
        this.roleId = roleId;
        this.roleName = roleName;
        this.status = status;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCatType() {
        return catType;
    }

    public void setCatType(String catType) {
        this.catType = catType;
    }

    public String getFunctionality() {
        return functionality;
    }

    public void setFunctionality(String functionality) {
        this.functionality = functionality;
    }

    public int getPageId() {
        return pageId;
    }

    public void setPageId(int pageId) {
        this.pageId = pageId;
    }

    public String getPageName() {
        return pageName;
    }

    public void setPageName(String pageName) {
        this.pageName = pageName;
    }

    public int getRoleId() {
        return roleId;
    }

    public void setRoleId(int roleId) {
        this.roleId = roleId;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
