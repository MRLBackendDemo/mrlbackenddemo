package com.mrlBackend.mrl.model;

import javax.persistence.*;
import java.sql.Date;
import java.util.Objects;

@Entity
@Table(name = "MRL_DMT_ROLE_LIST")
public class RoleList {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int roleId;

    @Column(name = "roleName")
    private String roleName;

    @Column(name = "status")
    private String status;

    @Column(name = "createDateTime")
    private Date createDataTime;

    public RoleList() {}

    public RoleList(int roleId, String roleName, String status, Date createDataTime) {
        this.roleId = roleId;
        this.roleName = roleName;
        this.status = status;
        this.createDataTime = createDataTime;
    }

    public int getRoleId() {
        return roleId;
    }

    public void setRoleId(int roleId) {
        this.roleId = roleId;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getCreateDataTime() {
        return createDataTime;
    }

    public void setCreateDataTime(Date createDataTime) {
        this.createDataTime = createDataTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RoleList roleList = (RoleList) o;
        return roleId == roleList.roleId && Objects.equals(roleName, roleList.roleName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(roleId, roleName);
    }

    @Override
    public String toString() {
        return "RoleList{" +
                "roleId=" + roleId +
                ", roleName='" + roleName + '\'' +
                ", status='" + status + '\'' +
                ", createDataTime=" + createDataTime +
                '}';
    }
}
