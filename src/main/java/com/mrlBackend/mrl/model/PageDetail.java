package com.mrlBackend.mrl.model;

import javax.persistence.*;

@Entity
@Table(name = "MRL_DMT_PAGE_LIST")
public class PageDetail {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @Column(name = "catType")
    private String catType;

    @Column(name = "functionality")
    private String functionality;

    @Column(name = "pageId")
    private int pageId;

    @Column(name = "pageName")
    private String pageName;

    @Column(name = "access")
    private String access;

    public PageDetail() {
    }

    public PageDetail(String catType, String functionality, int pageId, String pageName, String access) {
        this.catType = catType;
        this.functionality = functionality;
        this.pageId = pageId;
        this.pageName = pageName;
        this.access = access;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCatType() {
        return catType;
    }

    public void setCatType(String catType) {
        this.catType = catType;
    }

    public String getFunctionality() {
        return functionality;
    }

    public void setFunctionality(String functionality) {
        this.functionality = functionality;
    }

    public int getPageId() {
        return pageId;
    }

    public void setPageId(int pageId) {
        this.pageId = pageId;
    }

    public String getPageName() {
        return pageName;
    }

    public void setPageName(String pageName) {
        this.pageName = pageName;
    }

    public String getAccess() {
        return access;
    }

    public void setAccess(String access) {
        this.access = access;
    }
}
