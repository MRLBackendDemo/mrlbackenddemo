package com.mrlBackend.mrl.response;

public class RoleListResponse extends BaseResponse {

    private int roleId;

    public RoleListResponse() {
    }

    public RoleListResponse(int roleId) {
        this.roleId = roleId;
    }

    public int getRoleId() {
        return roleId;
    }

    public void setRoleId(int roleId) {
        this.roleId = roleId;
    }
}
