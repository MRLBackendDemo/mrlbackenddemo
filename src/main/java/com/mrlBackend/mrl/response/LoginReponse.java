package com.mrlBackend.mrl.response;

import java.util.List;

public class LoginReponse extends BaseResponse {
    List<Integer> roleIds;

    public List<Integer> getRoleIds() {
        return roleIds;
    }

    public void setRoleIds(List<Integer> roleIds) {
        this.roleIds = roleIds;
    }
}
