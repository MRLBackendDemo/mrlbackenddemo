package com.mrlBackend.mrl.response;

import com.mrlBackend.mrl.model.PageDetail;
import com.mrlBackend.mrl.model.PermissionDetail;

import java.util.List;

public class PermissionResponse extends BaseResponse {

    private List<PageDetail> pageDetailList;
    private List<PermissionDetail> permissionDetailList;

    public PermissionResponse() {
    }

    public PermissionResponse(List<PageDetail> pageDetailList, List<PermissionDetail> permissionDetailList) {
        this.pageDetailList = pageDetailList;
        this.permissionDetailList = permissionDetailList;
    }

    public List<PageDetail> getPageDetailList() {
        return pageDetailList;
    }

    public void setPageDetailList(List<PageDetail> pageDetailList) {
        this.pageDetailList = pageDetailList;
    }

    public List<PermissionDetail> getPermissionDetailList() {
        return permissionDetailList;
    }

    public void setPermissionDetailList(List<PermissionDetail> permissionDetailList) {
        this.permissionDetailList = permissionDetailList;
    }
}
