package com.mrlBackend.mrl.response;

public class UserResponse extends BaseResponse {

    private int userId;

    public UserResponse() {
    }

    public UserResponse(int userId) {
        this.userId = userId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }
}
