package com.mrlBackend.mrl.services;

import com.mrlBackend.mrl.response.*;
import com.mrlBackend.mrl.constants.StringConstants;
import com.mrlBackend.mrl.model.*;
import com.mrlBackend.mrl.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(StringConstants.baseUrl)
public class ActionControllerServices {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RoleListRepository roleListRepository;

    @Autowired
    private UserRoleMappingRepository userRoleMappingRepository;

    @Autowired
    private PermisionDetailRepository permisionDetailRepository;

    @Autowired
    private PageDetailRepository pageDetailRepository;

    @GetMapping(StringConstants.getAllUsers)
    public List<User> getAllUsers() {
        return userRepository.findAll();
    }

    @GetMapping(StringConstants.getUserById)
    public Optional<User> getUserById(@PathVariable int userId) {
        return userRepository.findById(userId);
    }

    @RequestMapping(value = StringConstants.register, method = {RequestMethod.POST, RequestMethod.GET})
    public BaseResponse register(@RequestBody User user) {
        BaseResponse response = new BaseResponse();
        response.setCode(StringConstants.Failure_Code);
        response.setMessage(StringConstants.Failure_Message);
        try {
            userRepository.save(user);
            response.setMessage(StringConstants.Success_Message);
            response.setCode(StringConstants.Success_Code);
        }
        catch (Exception e) {
            response.setMessage(StringConstants.Error_Saving_User_Msg);
            response.setMessage(StringConstants.Error_Saving_User_Code);
        }

        UserRoleMapping userRoleMapping = new UserRoleMapping();
        userRoleMapping.setRoleIds(0);
        userRoleMapping.setUserId(user.getUserId());

        try {
            userRoleMappingRepository.save(userRoleMapping);
            response.setCode(StringConstants.Success_Code);
            response.setMessage(StringConstants.Success_Message);
        }
        catch(Exception e) {
            response.setCode(StringConstants.Error_Saving_Role_Mapping_Code);
            response.setMessage(StringConstants.Error_Saving_Role_Mapping_Msg);
        }

        return response;
    }

    @RequestMapping(value = StringConstants.loginService, method = {RequestMethod.POST})
    public BaseResponse loginService(@RequestBody User userDetails) {
        LoginReponse response = new LoginReponse();
        response.setCode(StringConstants.Failure_Code);
        response.setMessage(StringConstants.Failure_Message);
        List<User> userFromDatabase = null;
        if(null != userDetails) {
            try {
                userFromDatabase = userRepository.findAll();
                for(User user : userFromDatabase) {
                    if(user.getUserEmail().equals(userDetails.getUserEmail())) {
                        userDetails.setUserId(user.getUserId());
                    }
                }
            }
            catch(Exception e) {
                response.setMessage(StringConstants.Exception_Fetching_DB_Msg);
                response.setCode(StringConstants.Exception_Fetching_DB_Code);
                return response;
            }
        }

        List<UserRoleMapping> userRoleMappingList = null;
        try {
            userRoleMappingList = userRoleMappingRepository.findAll();
        }
        catch (Exception e) {
            response.setCode(StringConstants.Exception_Fetching_DB_Code);
            response.setMessage(StringConstants.Exception_Fetching_DB_Msg);
        }

        if(null != userFromDatabase) {
            if(userFromDatabase.contains(userDetails)) {
                List<Integer> roleIdTemp = new ArrayList<Integer>();
                for(UserRoleMapping userRoleMapping : userRoleMappingList) {
                    if(userDetails.getUserId() == userRoleMapping.getUserId()) {
                        roleIdTemp.add(userRoleMapping.getRoleIds());
                    }
                }
                response.setCode(StringConstants.Success_Code);
                response.setMessage(StringConstants.Success_Message);
                response.setRoleIds(roleIdTemp);
                return response;
            }
            else {
                response.setMessage(StringConstants.Invalid_User_Msg);
                response.setMessage(StringConstants.Invalid_User_Code);
                return response;
            }
        }
        return response;
    }

    @GetMapping(StringConstants.Get_Role_List)
    public List<RoleList> getRoleList() {
        BaseResponse response = new BaseResponse();
        response.setCode(StringConstants.Success_Code);
        response.setCode(StringConstants.Success_Message);
        List<RoleList> roleLists = null;
        try {
            roleLists = roleListRepository.findAll();
        }catch (Exception e) {
            response.setCode(StringConstants.Exception_Fetching_DB_Code);
            response.setMessage(StringConstants.Exception_Fetching_DB_Msg);
        }
        return roleLists;
    }

    @GetMapping(StringConstants.Get_User_List)
    public List<User> getUserList() {
        BaseResponse response = new BaseResponse();
        response.setCode(StringConstants.Success_Code);
        response.setMessage(StringConstants.Success_Message);
        List<User> userList = null;
        try {
            userList = userRepository.findAll();
        }
        catch (Exception e) {
            response.setMessage(StringConstants.Exception_Fetching_DB_Msg);
            response.setCode(StringConstants.Exception_Fetching_DB_Code);
        }

        List<UserRoleMapping> userRoleMappingList = null;
        try{
            userRoleMappingList = userRoleMappingRepository.findAll();
        }
        catch (Exception e) {
            response.setCode(StringConstants.Exception_Fetching_DB_Code);
            response.setMessage(StringConstants.Exception_Fetching_DB_Msg);
        }

        List<RoleList> roleLists = null;
        try {
            roleLists = roleListRepository.findAll();
        }
        catch (Exception e) {
            response.setMessage(StringConstants.Exception_Fetching_DB_Msg);
            response.setCode(StringConstants.Exception_Fetching_DB_Code);
        }

        return userList;
    }

    @RequestMapping(value = StringConstants.Add_Update_User, method = {RequestMethod.POST, RequestMethod.PUT})
    public BaseResponse addUpdateUser(@RequestBody User userDetails) {
        UserResponse response = new UserResponse();
        response.setCode(StringConstants.Failure_Code);
        response.setMessage(StringConstants.Failure_Message);
        try {
            Optional<User> userTemp = userRepository.findById(userDetails.getUserId());
            if(userTemp.isPresent()) {
                User updateUser = getUpdatedUser(userTemp, userDetails);
                try {
                    userRepository.save(updateUser);
                    response.setMessage(StringConstants.Success_Message);
                    response.setCode(StringConstants.Success_Code);
                    response.setUserId(updateUser.getUserId());
                }
                catch(Exception e) {
                    response.setMessage(StringConstants.Error_Updating_User_Msg);
                    response.setCode(StringConstants.Error_Updating_User_Code);
                }
                return response;
            }
            else if(null != userDetails) {
                try {
                    userRepository.save(userDetails);
                    response.setMessage(StringConstants.Success_Message);
                    response.setCode(StringConstants.Success_Code);
                    response.setUserId(userDetails.getUserId());
                }
                catch(Exception e) {
                    response.setMessage(StringConstants.Error_Saving_User_Msg);
                    response.setCode(StringConstants.Error_Saving_User_Code);
                }
                return response;
            }
        }
        catch (Exception e) {
            response.setMessage(StringConstants.User_Getting_Error_Msg);
            response.setCode(StringConstants.User_Getting_Error_Code);
        }

        return response;
    }

    @RequestMapping(value = StringConstants.Add_Update_Role, method = {RequestMethod.POST})
    public BaseResponse addRole(@RequestBody RoleList roleListDetails) {
        RoleListResponse response = new RoleListResponse();
        response.setCode(StringConstants.Failure_Code);
        response.setMessage(StringConstants.Failure_Message);
        if(null != roleListDetails) {
            try {
                roleListRepository.save(roleListDetails);
                response.setMessage(StringConstants.Success_Message);
                response.setCode(StringConstants.Success_Code);
                response.setRoleId(roleListDetails.getRoleId());
            }
            catch(Exception e) {
                response.setMessage(StringConstants.Error_Saving_Role_Msg);
                response.setCode(StringConstants.Error_Saving_Role_Code);
            }
        }

        return response;
    }

    @RequestMapping(value = StringConstants.Add_Update_Role, method = {RequestMethod.PUT})
    public BaseResponse updateRole(@RequestBody RoleList roleListDetails) {
        RoleListResponse response = new RoleListResponse();
        response.setCode(StringConstants.Failure_Code);
        response.setMessage(StringConstants.Failure_Message);
        try {
            Optional<RoleList> roleListTemp = roleListRepository.findById(roleListDetails.getRoleId());
            if(roleListTemp.isPresent()) {
                RoleList updateRole = getUpdatedRole(roleListTemp, roleListDetails);
                try {
                    roleListRepository.save(updateRole);
                    response.setMessage(StringConstants.Success_Message);
                    response.setCode(StringConstants.Success_Code);
                    response.setRoleId(updateRole.getRoleId());
                }
                catch(Exception e) {
                    response.setMessage(StringConstants.Error_Updating_Role_Msg);
                    response.setCode(StringConstants.Error_Updating_Role_Code);
                }
                return response;
            }
        }
        catch(Exception e) {
            response.setCode(StringConstants.Role_Does_Not_Exist_Code);
            response.setMessage(StringConstants.Role_Does_Not_Exist_Msg);
        }

        return response;
    }

    @GetMapping(StringConstants.Get_Permission_List)
    public PermissionResponse getPermissionList(@RequestParam List<Integer> roleIds) {
        PermissionResponse response = new PermissionResponse();
        response.setMessage(StringConstants.Failure_Message);
        response.setCode(StringConstants.Failure_Code);
        List<PageDetail> pageDetailList = new ArrayList<PageDetail>();
        try {
            pageDetailList = pageDetailRepository.findAll();
        }
        catch(Exception e) {
            response.setCode(StringConstants.Error_Fetching_Page_List_Details_Code);
            response.setMessage(StringConstants.Error_Fetching_Page_List_Details_Msg);
        }

        List<PermissionDetail> permissionDetailList = new ArrayList<PermissionDetail>();
        try {
            permissionDetailList = permisionDetailRepository.findAll();
        }
        catch (Exception e) {
            response.setCode(StringConstants.Error_Fetching_Permission_List_Code);
            response.setMessage(StringConstants.Error_Fetching_Permission_List_Msg);
        }

        List<PermissionDetail> permissionDetailListTemp = new ArrayList<PermissionDetail>();
        List<Integer> pageIdList = new ArrayList<Integer>();
        for(int roleId : roleIds) {
            if(roleId == StringConstants.adminRoleId) {
                List<PageDetail> pageDetailListTemp = new ArrayList<PageDetail>();
                for(PageDetail pageDetail : pageDetailList) {
                    if(pageDetail.getAccess().equals(StringConstants.Admin_Access_Code)) {
                        pageIdList.add(pageDetail.getPageId());
                        pageDetailListTemp.add(pageDetail);
                    }
                }
                response.setPageDetailList(pageDetailListTemp);
                response.setPermissionDetailList(permissionDetailList);
                response.setCode(StringConstants.Success_Code);
                response.setMessage(StringConstants.Success_Message);
            }
            else if(roleId != StringConstants.adminRoleId) {
                for(PageDetail pageDetail : pageDetailList) {
                    if(pageDetail.getAccess().equals(StringConstants.User_Access_Code)) {
                        pageIdList.add(pageDetail.getPageId());
                    }
                }
                for(PermissionDetail permissionDetail : permissionDetailList) {
                    if(permissionDetail.getStatus().equals(StringConstants.Permission_Detail_Status_OK) && pageIdList.contains(permissionDetail.getPageId()) && roleId == permissionDetail.getRoleId()) {
                        permissionDetailListTemp.add(permissionDetail);
                    }
                }
                response.setPermissionDetailList(permissionDetailListTemp);
                response.setCode(StringConstants.Success_Code);
                response.setMessage(StringConstants.Success_Message);
            }
        }
        return response;
    }

    @RequestMapping(value = StringConstants.Update_Permission_List, method = {RequestMethod.PUT})
    public BaseResponse updatePermissionList(@RequestBody PermissionDetail permissionDetail, @PathVariable int id) {
        BaseResponse response = new BaseResponse();
        response.setMessage(StringConstants.Failure_Message);
        response.setCode(StringConstants.Failure_Code);
        Optional<PermissionDetail> permissionDetailTemp = permisionDetailRepository.findById(id);
        if(permissionDetailTemp.isPresent()) {
            PermissionDetail saveNewPermissionDetail = getPermissionDetailObject(permissionDetail, id,
                    permissionDetailTemp);
            try {
                permisionDetailRepository.save(saveNewPermissionDetail);
                response.setMessage(StringConstants.Success_Message);
                response.setCode(StringConstants.Success_Code);
            }
            catch(Exception e) {
                response.setCode(StringConstants.Error_Saving_Permission_Detail_Code);
                response.setMessage(StringConstants.Error_Saving_Permission_Detail_Msg);
            }

        }
        return response;
    }

    private PermissionDetail getPermissionDetailObject(PermissionDetail permissionDetail, int id,
                                        Optional<PermissionDetail> permissionDetailTemp) {
        permissionDetail.setId(id);
        permissionDetail.setCatType(permissionDetailTemp.get().getCatType());
        permissionDetail.setFunctionality(permissionDetailTemp.get().getFunctionality());
        permissionDetail.setRoleId(permissionDetailTemp.get().getRoleId());
        permissionDetail.setStatus(permissionDetail.getStatus());
        permissionDetail.setPageId(permissionDetailTemp.get().getPageId());
        permissionDetail.setPageName(permissionDetailTemp.get().getPageName());
        permissionDetail.setRoleName(permissionDetailTemp.get().getRoleName());
        return permissionDetail;
    }

    private RoleList getUpdatedRole(Optional<RoleList> roleListTemp,RoleList roleListDetails) {
        roleListTemp.get().setRoleName(roleListDetails.getRoleName());
        roleListTemp.get().setStatus(roleListDetails.getStatus());
        roleListTemp.get().setCreateDataTime(roleListDetails.getCreateDataTime());
        return roleListTemp.get();
    }

    private User getUpdatedUser(Optional<User> userTemp, User userDetails) {
        userTemp.get().setUserEmail(userDetails.getUserEmail());
        userTemp.get().setUserPassword(userDetails.getUserPassword());
        userTemp.get().setStatus(userDetails.getStatus());
        userTemp.get().setCreateDateTime(userDetails.getCreateDateTime());
        return userTemp.get();
    }
}
