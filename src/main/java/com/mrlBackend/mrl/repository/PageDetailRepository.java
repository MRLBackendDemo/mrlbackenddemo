package com.mrlBackend.mrl.repository;

import com.mrlBackend.mrl.model.PageDetail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PageDetailRepository extends JpaRepository<PageDetail, Integer> {
}
