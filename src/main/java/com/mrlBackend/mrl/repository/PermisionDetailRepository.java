package com.mrlBackend.mrl.repository;

import com.mrlBackend.mrl.model.PermissionDetail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PermisionDetailRepository extends JpaRepository<PermissionDetail, Integer> {
}
